from tkinter import Tk, Button, Label, messagebox, filedialog
from multiprocessing import Pool, cpu_count, freeze_support
from shutil import copyfile
import time
from PIL import Image, ImageTk, ImageOps
import glob
import os
import sys

def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        print("{} function took {:0.3f} ms".format(f.func_name, (time2 - time1) * 1000.0))
        return ret

    return wrap


SCALE_FACTOR = 5
MAX_PICS = SCALE_FACTOR
THUMB_STRING = 'thumb{}'
FOLDER_NAME = "selectedImages"
VALID_IMAGE_TYPES = ('jpg', 'png', 'jpeg', 'bmp')


def createThumb(imageName, scaleFactor, save=False):
    image = Image.open(imageName)
    image.thumbnail((image.width / scaleFactor, image.height / scaleFactor), Image.ANTIALIAS)
    if save:
        image.save(THUMB_STRING.format(imageName))
    return image


def initThumg(infoImage):
    allImages = glob.glob(THUMB_STRING.format('*'))
    imageName = infoImage[0]
    scaleFactor = infoImage[1]
    if THUMB_STRING.format(imageName) not in allImages:
        createThumb(imageName, scaleFactor, True)
        sys.stdout.write('.')


class OrderPicsProgramm(object):
    def __init__(self):
        self.imageType = None
        # load the already selected arrow
        self.okBig = Image.open('src/okArrow.png').convert("RGBA").resize((50, 50), Image.BILINEAR)
        self.okSmall = Image.open('src/okArrow.png').convert("RGBA").resize((25, 25), Image.BILINEAR)
        self.photoDisplayTKWindow = Tk(className="Order picture")
        self.photoDisplayTKWindow.withdraw()
        self.pathObj = filedialog.askopenfilenames()
        if not self.pathObj:
            self.closeProgram()
        self.imageType = self.pathObj[0].split('.')[-1]
        self.path = os.path.dirname(os.path.realpath(self.pathObj[0]))
        os.chdir(self.path)
        self.pool = Pool(int(cpu_count() / 2))
        # load all filenames except the thumbs
        self.arrPics = sorted([os.path.basename(pic) for pic in self.pathObj if 'thumb' not in pic and self.imageType in pic])
        self.checkExtensionOfFiles()
        self.maxValue = len(self.arrPics)
        self.photoPointer = 0
        # create the control window and let it disappear
        self.controlTKWindow = Tk(className="Control")
        self.controlTKWindow.withdraw()
        self.prevBtn = Button(self.controlTKWindow, text="Prev", command=self.prevPic)
        self.prevBtn.grid(row=1, column=0)
        self.selectBtn = Button(self.controlTKWindow, text="Select", command=self.select)
        self.selectBtn.grid(row=1, column=1)
        self.nextBtn = Button(self.controlTKWindow, text="Next", command=self.nextPic)
        self.nextBtn.grid(row=1, column=2)
        # bind the keyboard key to the actions
        self.controlTKWindow.bind('<Left>', self.arrowLeft)
        self.controlTKWindow.bind('<Right>', self.arrowRight)
        self.controlTKWindow.bind('<space>', self.space)

        self.imageInSelectedPics = sorted(glob.glob(os.path.join(self.path, FOLDER_NAME, '*.' + self.imageType)))
        self.image = None
        self.listOfThumbs = []
        self.createFolder()
        # the label shows the image
        self.label = Label(self.photoDisplayTKWindow)
        # select a callback if a window is closed
        self.controlTKWindow.protocol('WM_DELETE_WINDOW', self.closeProgram)
        self.photoDisplayTKWindow.protocol('WM_DELETE_WINDOW', self.closeProgram)
        print("Creating thumbs in an own process")
        self.initThumbs()
        self.changePic()
        # let the windows appear
        self.photoDisplayTKWindow.deiconify()
        self.controlTKWindow.deiconify()

    def checkExtensionOfFiles(self):
        print(self.arrPics)
        result = all(True if self.imageType in image else False for image in self.arrPics)
        if not result:
            messagebox.showerror('File extension error', 'Not all files have got the same file extension!')
            self.closeProgram()

        if len(self.arrPics) < 5:
            messagebox.showerror('Amount error', 'At least 5 files have to be selected!')
            self.closeProgram()

        if self.imageType not in VALID_IMAGE_TYPES and self.imageType not in [imType.upper() for imType in
                                                                              VALID_IMAGE_TYPES]:
            messagebox.showerror('Image type error',
                                   'Not a valid image extensions! Valid are {}.'.format(', '.join(VALID_IMAGE_TYPES)))
            self.closeProgram()

    def closeProgram(self):
        # if not all thumbs are created the process will be stop here
        self.pool.terminate()
        # delete all thumbs
        _ = [os.remove(pic) for pic in glob.glob(THUMB_STRING.format("*"))]
        # kill the window processes
        try:
            self.controlTKWindow.destroy()
            self.photoDisplayTKWindow.destroy()
        except AttributeError:
            pass
        sys.exit()

    def buildString(self):
        # create an info string for the main window
        return "{} of {} images, already selected {} images".format(self.photoPointer + 1, len(self.arrPics),
                                                                    len(self.imageInSelectedPics))

    def initThumbs(self):
        # create thumb of every single image
        self.pool.map_async(initThumg, zip(self.arrPics, len(self.arrPics) * (SCALE_FACTOR * SCALE_FACTOR,)))

    def changePic(self):
        # set a new windows title
        self.photoDisplayTKWindow.wm_title(self.buildString())
        self.image = self.createThumbs([self.arrPics[self.photoPointer]], True, SCALE_FACTOR)

        if self.isTheImageAlreadySelected(self.arrPics[self.photoPointer]):
            self.image.paste(self.okBig, (0, 0), self.okBig)
        self.createThumbs(self.arrPics[self.photoPointer:self.photoPointer + 5], False, SCALE_FACTOR * SCALE_FACTOR)
        self.addThumbsToImage()
        self.showPic()

    def showPic(self):
        photo = ImageTk.PhotoImage(self.image)
        self.label = Label(self.photoDisplayTKWindow, image=photo)
        self.label.image = photo  # keep a reference!
        self.label.grid(row=0, column=1)

    def isTheImageAlreadySelected(self, fileName):
        self.imageInSelectedPics = os.listdir(os.path.join(self.path, FOLDER_NAME))
        return True if fileName in self.imageInSelectedPics else False

    @staticmethod
    def createFolder():
        if not os.path.exists(FOLDER_NAME):
            os.makedirs(FOLDER_NAME)

    def select(self):
        if not self.isTheImageAlreadySelected(self.arrPics[self.photoPointer]):
            copyfile(self.arrPics[self.photoPointer], os.path.join(FOLDER_NAME, self.arrPics[self.photoPointer]))
        else:
            directory = os.getcwd()
            os.chdir(FOLDER_NAME)
            os.remove("{}".format(self.arrPics[self.photoPointer]))
            os.chdir(directory)
            self.image = Image.open(self.arrPics[self.photoPointer])
        self.changePic()

    def nextPic(self):
        self.label.destroy()
        self.photoPointer += 1
        if self.photoPointer >= self.maxValue:
            self.photoPointer = 0
        self.image = Image.open(self.arrPics[self.photoPointer])
        self.changePic()

    def arrowLeft(self, _):
        self.prevPic()

    def arrowRight(self, _):
        self.nextPic()

    def space(self, _):
        self.select()

    def prevPic(self):
        self.label.destroy()
        self.photoPointer -= 1
        if self.photoPointer < 0:
            self.photoPointer = self.maxValue - 1
        self.image = Image.open(self.arrPics[self.photoPointer])
        self.changePic()

    def createThumbs(self, listOfFileNames, singleImage, scaleFactor):
        self.listOfThumbs = []
        counter = 0
        # add 5 photo to the preview if the end of the photo range is reached
        while len(listOfFileNames) < 5 and not singleImage:
            listOfFileNames.append(self.arrPics[counter])
            counter += 1

        if singleImage:
            return createThumb(listOfFileNames[0], scaleFactor)
        while (True):
            # try to create preview of the thumbs
            # they were creating in a own process therefore it might be took a few seconds
            try:
                self.listOfThumbs = [self.addArrowToThumb(Image.open(THUMB_STRING.format(image))) for image in
                                     listOfFileNames]
                return None
            except IOError:
                pass

    def addArrowToThumb(self, imageObj):
        if self.isTheImageAlreadySelected(imageObj.filename.lstrip(THUMB_STRING.format(''))):
            imageObj.paste(self.okSmall, (0, 0), self.okSmall)
        return imageObj

    def addThumbsToImage(self):
        # empty image
        imageThumbs = Image.new("RGBA", (MAX_PICS * self.listOfThumbs[0].width, self.listOfThumbs[0].height),
                                4 * (255,))
        # empty image with the dimension of the preview and the selected photo
        backGround = Image.new("RGBA", (self.image.size[0], self.image.size[1] + imageThumbs.size[1]))
        # add the thumbs
        _ = [imageThumbs.paste(self.listOfThumbs[i], (i * self.listOfThumbs[0].size[0], 0)) for i in range(MAX_PICS)]
        # add a frame to the preview
        imageThumbs = ImageOps.expand(imageThumbs, border=2, fill="gray")
        backGround.paste(self.image, (0, 0))
        backGround.paste(imageThumbs, (0, self.image.size[1]))
        self.image = backGround


if __name__ == '__main__':
    freeze_support()
    app = OrderPicsProgramm()
    app.controlTKWindow.mainloop()
