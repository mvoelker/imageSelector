This python app is used to select photos within a TkInter GUI

I didn't found any program which could be used to select photo from a big set of photos. Therefore I wrote my own application. 
If you have a similar use case try it out and report any bug. The preview will work best with a same format of the photos. 

This is the first version of this python app. Until now only python 2.7. is supported, the packages PIL/Pillow and TKinter are also needed. The next step is to add python 3 compatibilty. 
The program will ask for a specific set of photos to load them. There are two windows, one is used to display the selected image and the other is used to navigate
through the images. If the control is selected you could useleft, right and space keyboard keys to navigate.